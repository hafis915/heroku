'use strict';
const data = require("../data/class.json")
data.forEach(el => {
  el.createdAt = new Date()
  el.updatedAt = new Date()
})
console.log(data)
module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.bulkInsert("Classes", data, {})
  },

  down: async (queryInterface, Sequelize) => {
    await queryInterface.bulkDelete('Classes', null, {});
  }
};
