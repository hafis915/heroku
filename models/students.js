'use strict';
const uuid = require("uuid")

const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class Students extends Model {
    static associate(models) {
      Students.hasMany(models.Vehicles)
      // Students.hasMany(models.StudentClass)
      Students.belongsToMany(models.Class, { through: models.StudentClass })
      // define association here
    }
  };
  Students.init({
    id: {
      primaryKey: true,
      type: DataTypes.STRING
    },
    name: DataTypes.STRING,
    dom: {
      type: DataTypes.STRING,
      validate: {
        len: {
          msg: "Jumlah suku kata minimal 4  dan max 8",
          args: [4, 8]
        },
        gkBolehHurufA(value) {
          console.log(value, "<<<< INI VALUE")
          if (value[0].toLowerCase() === 'a') {
            throw new Error('Gk boleh di awali dengan huruf A');
          }
        }
      }
    },
    age: {
      type: DataTypes.STRING,
      validate: {
        min: {
          msg: "Minimal 18 Tahun ",
          args: 18
        }
      }
    },
    isGraduate: DataTypes.BOOLEAN,
    Password: DataTypes.STRING,
  },
    {
      //cara 1
      hooks: {
        beforeCreate: (instance, options) => {
          const id = uuid.v4()
          if (!instance.dom) {
            instance.dom = "Earth"
          }
          instance.id = id
          // console.log(options, "<<<<< Options")
        },
      },
      sequelize,
      modelName: 'Students',
    });

  //cara 2
  // Students.addHook('beforeDestroy', (inst, opt) => {

  // })
  //cara 3
  // Students.beforeUpdate((ins, opt) => {

  // })
  return Students;
};