const app = require("../app")
const request = require("supertest")
const { Class } = require("../models")

const payload_success_creaete_class = {
  Name: "Math",
  Day: "Sunday"
}

beforeAll(async () => {
  // Logic yang akan dijalankan sebelum semua describe dijalankan
  console.log("==== Before All ====")
})

afterAll(async () => {
  // Logic yang akan dijalankan setelah semua describ dijalankan
  await Class.destroy({ where: {} })


})


// Skenario yang akan dijalankan untuk testing
describe("Success", () => {
  describe("Success Create Clas", () => {
    it("Succes Create catalog", (done) => {
      request(app)
        .post("/class") // method (url)
        .send(payload_success_creaete_class) // Body(payload)
        // .set() // Headers(key, value)
        .end((err, res) => {
          if (err) {
            done(err)
          }

          const { body, status } = res
          expect(status).toBe(200)
          expect(body).toHaveProperty("data")
          expect(body.data).toHaveProperty("Name", payload_success_creaete_class.Name)
          expect(body.data).toHaveProperty("Day", payload_success_creaete_class.Day)
          done()
        })
    })
  })
})

// describe("Failed Case", () => {

// })