const { Students, Class } = require("../models")

class ClassController {
  static async getClasses(req, res) {
    try {
      const options = {
        include: [
          {
            model: Students,
            // attributes: ["name"]
          }
        ]
      }
      const data = await Class.findAll(options)
      res.status(200).json({ data })
    } catch (error) {
      console.log(error)
    }

  }

  static async getClass(req, res) {
    const id = req.params.id
    const options = {
      where: {
        id
      },
    }
    const data = await Class.findOne(options)

    res.status(200).json({ data })
  }

  static async updateClass(req, res) {
    const { Name, Day } = req.body

    const payload = {
      Name, Day
    }
    const id = req.params.id
    const options = {
      where: {
        id
      },
      returning: true
    }
    const updated = await Class.update(payload, options)

    res.status(200).json({
      data: updated
    })
  }

  static async createClass(req, res) {
    const { Name, Day } = req.body
    const payload = {
      Name, Day
    }

    const newClass = await Class.create(payload)
    res.status(200).json({ data: newClass })
  }


  static async deleteClass(req, res) {
    const id = req.params.id
    const deleted = await Class.destroy({
      where: {
        id
      },
      returning: true
    })

    res.status(200).json({
      data: deleted
    })
  }
}

module.exports = ClassController