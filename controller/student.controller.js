const { Students, Vehicles, Class } = require("../models")
const { token, encrypt } = require("../utils")

class StudentController {
  static async login(req, res) {
    const { name, password } = req.body
    const student = await Students.findOne({
      where: {
        name
      }
    })
    const compare = student ? encrypt.isPwdValid(password, student.dataValues.Password) : 0
    console.log(student)
    // const checkPassword = student ? student.dataValues.Password === encryptPass : 0
    if (!student || !compare) {
      // res.status(401).json({
      //   statuses: 401,
      //   message: "Unauthorize access"
      // })
      // return
    }
    const student = req.Login
    const access_token = token.makeToken(student.dataValues)
    res.status(200).json({ access_token })



  }

  static async getStudents(req, res) {
    try {
      const options = {
        include: [
          {
            model: Vehicles,
            attributes: ["type"]
          },
          {
            model: Class,
            attributes: ['Name', 'Day']
          }
        ]
      }
      const data = await Students.findAll(options)
      res.status(200).json({ data })
    } catch (error) {
      console.log(error)
    }

  }

  static async getStudent(req, res) {
    const id = req.params.id
    const options = {
      where: {
        id
      },
      include: [
        {
          model: Vehicles
        }
      ]
    }
    const data = await Students.findOne(options)

    res.status(200).json({ data })
  }

  static async updateStudent(req, res) {
    const { name, dom, isGraduate, age } = req.body

    const payload = {
      name, dom, isGraduate, age
    }
    const id = req.params.id
    const options = {
      where: {
        id
      },
      returning: true
    }
    const updated = await Students.update(payload, options)

    res.status(200).json({
      data: updated
    })
  }

  static async createStudent(req, res) {
    try {
      let { name, dom, isGraduate, age, Password } = req.body
      Password = encrypt.encryptPwd(Password)
      const payload = {
        name, dom, isGraduate, age, Password
      }

      const newStudent = await Students.create(payload)
      res.status(200).json({ data: newStudent })
    } catch (error) {
      res.status(200).json({
        msg: error.message
      })
    }

  }


  static async deleteStudent(req, res) {
    console.log("=== delete student ====")
    const id = req.params.id
    const deleted = await Students.destroy({
      where: {
        id
      },
      returning: true
    })

    res.status(200).json({
      data: deleted
    })
  }
}

module.exports = StudentController