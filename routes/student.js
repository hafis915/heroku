const { StudentController } = require("../controller")
const route = require("express").Router()
const { authentication, studentAuthorization } = require("../Middleware")

route.get("/", StudentController.getStudents)
route.get("/:id", StudentController.getStudent)
route.post('/login', StudentController.login)
route.post("/", StudentController.createStudent)

// route.use(authentication)

route.put("/:id", StudentController.updateStudent)
// route.put("/:id", studentAuthorization, StudentController.updateStudent)
route.delete("/:id", studentAuthorization, StudentController.deleteStudent)


module.exports = route