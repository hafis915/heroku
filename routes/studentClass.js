const { StudentClassController } = require("../controller")
const route = require("express").Router()
const { authentication } = require("../Middleware")


route.get("/", StudentClassController.getStudentClasses)
route.get("/:id", StudentClassController.getStudentClass)
route.use(authentication)
route.post("/", StudentClassController.createStudentClass)
route.put("/:id", StudentClassController.updateStudentCLass)
route.delete("/:id", StudentClassController.deleteStudentClass)


module.exports = route