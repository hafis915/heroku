const bcrypt = require('bcryptjs');

class Encryption {
  static encryptPwd(raw) {
    try {
      const salt = bcrypt.genSaltSync(10);
      const hash = bcrypt.hashSync(raw, salt);
      console.log(hash)
      return hash;
    } catch (error) {
      return null
    }

  }

  static isPwdValid(raw, hash) {
    try {
      return bcrypt.compareSync(raw, hash)
    } catch (error) {
      return null
    }
  }
}

module.exports = Encryption;
